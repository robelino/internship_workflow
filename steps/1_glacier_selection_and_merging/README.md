# Glacier group outlines processing & selection

The Randolph Glacier Inventory (RGI) is the baseline of the workflow. The entities to be modelled will be selected based on the outlines of the dataset. However, some glacier that are separated in the inventory (for historical or practical reasons) are in reality a single continuous ice body. We thus need to merge them into one entity for Elmer/Ice to work properly.

#FILES
**grouping_shp.py** is the script to run in order to merge the glacier entities. It creates a new attribute for every glacier (*glacier_gr*) that is its *group number* (Every entity in a group shares the same). It need to be run with the RGI region number of the glaciers we want to model as an argument (eg. *01* for Alaska, *11* for Central Europe..).

**glacier_group_list** is the text file that needs to be filled with the *group number* of every group we want to model (this selection can be automated by a script if wanted). It we be called by future steps.

**rgi_grouped/** is a directory containing the merged rgi shapefile once *grouping_shp.py* has been run.

#TO PROCESS THE STEP

1. run 
```
grouping_shp.py region_nb
```

2. write the glacier groups you want to model in *glacier_group_list.txt*




#Warning
Some glacier groups can be very large: it can rise some problems with Elmer/Ice when solving the Stokes equations. However, it is possible to manually split them where they don't have a lot of interactions (e.g., if glacier join at the top of a ridge).
