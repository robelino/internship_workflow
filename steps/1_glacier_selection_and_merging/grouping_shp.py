#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#=========================================
# AUTHOR:   D. MAURE
# ORGANIZATION: IGE(CNRS-France)
#
# VERSION: V1 
# CREATED: 2021-04
# MODIFIED: 
#  
#
#========================================== 
############################

import geopandas as gpd
import os
import sys

regions=[sys.argv[0]]
folder_path = "../../data/00_rgi60/"
for region in regions:
    region_name = str(region) +'_rgi60_'
    for fname in os.listdir(folder_path): 
        print(fname)
        if (region_name in fname) and ~('grouped' in fname):
            region_name = fname
        region_path = folder_path + region_name
    shp = gpd.read_file(region_path)
    #overlap_matrix = shp.apply(lambda x: shp.overlaps(x)).values.astype(int)
    groups=gpd.GeoDataFrame(geometry=list(shp.unary_union))
    
    groups['glacier_group']=groups.index+1
    shp=gpd.tools.sjoin(shp, groups, how='left').drop('index_right',1)

    shp.to_file('rgi_grouped/rgi.shp')

