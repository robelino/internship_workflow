function ncmin { ncap2 -O -C -v -s "foo=${1}.min();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }
function ncmax { ncap2 -O -C -v -s "foo=${1}.max();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }

export HOME_DIR=$(pwd)
data_path=$HOME_DIR/../../data
bed_path=$HOME_DIR/../2_DEMs_merging
shp_path=$HOME_DIR/../../data

global_data_path=$data_path/surfDEMs/rgi_glob.tif

file='../1_glacier_selection_and_merging/glacier_group_list.txt'
A_list=$HOME_DIR'/supplementary_material/A_list.txt'
C_list=$HOME_DIR'/supplementary_material/C_list.txt'
while read line;do	
	echo "$line"
	mkdir dirs/$line
	cd dirs/
	cd $line
		while read A; do
		mkdir $A
		cd $A
			while read C;do
			mkdir $C
			cd $C
	for model_path in $data_path/thickDEMs/*;do	
		model=$(echo $model_path | rev | cut -d'/' -f 1| rev)
		#if [ $model == 'r_model' ] ;
		#then #to delete

		mkdir $model
		mkdir $model/INPUT
		mkdir $model/OUTPUT

		cd $model
		#gdal_translate -of netcdf $data_path/surfDEMs/surface_DEM_$line.tif INPUT/surface.nc
		#gdal_translate -of netcdf $data_path/thickDEMs/${line}_thickness.tif INPUT/thick.nc
#import composite thickness
		cp $bed_path/DEMs/bedrock/$line/$model/merged.tif INPUT/thick.tif
		gdal_translate -of netcdf INPUT/thick.tif INPUT/thick.nc
#import composite surface
		#cp $data_path/surfDEMs/surface_DEMs_RGI60-11_grouped/$line/surface.nc INPUT/surface.nc
#import global surface
		
		cs=25
		xmin0=$(ncmin x INPUT/thick.nc)
		xmax0=$(ncmax x INPUT/thick.nc)
		ymin0=$(ncmin y INPUT/thick.nc)
		ymax0=$(ncmax y INPUT/thick.nc)
		
		xmin=$(echo "scale=10; $xmin0-$cs/2" | bc -l)
		ymin=$(echo "scale=10; $ymin0-$cs/2" | bc -l)
		xmax=$(echo "scale=10; $xmax0+$cs/2" | bc -l)
		ymax=$(echo "scale=10; $ymax0+$cs/2" | bc -l)

		echo $xmin $xmax $ymin $ymax
		
		gdalwarp -ot Float32 -s_srs EPSG:4326 -t_srs EPSG:32632 -r bilinear -of GTiff -tr $cs $cs -te $xmin $ymin $xmax $ymax -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -wo OPTIMIZE_SIZE=TRUE $global_data_path INPUT/surface.tif
		gdal_translate -of netcdf INPUT/surface.tif INPUT/surface.nc
		#gdal_translate -of netcdf $data_path/surfDEMs_glob/11-mtblanc.tif INPUT/toclip.nc
		#gdaltindex INPUT/clip.shp INPUT/thick.nc
		#gdalwarp  -r cubic -tr 25 -25 -cutline INPUT/clip.shp -crop_to_cutline INPUT/toclip.nc INPUT/surface.nc
		
		#gdalwarp -ot Float32 -t_srs EPSG:32633 -r cubic -of GTiff -tr 25 25 -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -wo OPTIMIZE_SIZE=TRUE $HOME_DIR/Data/surfDEMs_glob/11-mtblanc.tif INPUT/toclip.tif
		#gdal_translate -of netcdf INPUT/toclip.tif INPUT/toclip.nc

		#gdalwarp -cutline INPUT/clip.shp -crop_to_cutline INPUT/toclip.tif INPUT/surface.tif

	    	ncrename -v Band1,surface INPUT/surface.nc
		ncrename -v Band1,thick  INPUT/thick.nc
		#gdal_merge.py -a_nodata -9999 -ot Float32 -separate -o INPUT/surface.nc -of GTif INPUT/surface.nc INPUT/thick.nc
		ncks -A INPUT/thick.nc INPUT/surface.nc
		ncap2 -s "bed=surface-thick" INPUT/surface.nc INPUT/DEMs.nc


		

		xmin=$(ncmin x INPUT/DEMs.nc)
		xmax=$(ncmax x INPUT/DEMs.nc)
		ymin=$(ncmin y INPUT/DEMs.nc)
		ymax=$(ncmax y INPUT/DEMs.nc)

		res=100.0

		echo $xmin $xmax $ymin $ymax
		
		cp $HOME_DIR/supplementary_material/rectangle.geo INPUT/rectangle.geo

		gmsh -1 -2 INPUT/rectangle.geo -setnumber xmin $xmin -setnumber xmax $xmax \
					 -setnumber ymin $ymin -setnumber ymax $ymax \
					 -setnumber lc $res 

		ElmerGrid 14 2 INPUT/rectangle.msh -autoclean
		ElmerGrid 2 5 INPUT/rectangle
		ElmerGrid 14 2 INPUT/rectangle.msh -metis 8 -autoclean
		cd ..
		
#fi
	done
	cd ..
	done < $C_list
	cd ..
	done < $A_list
	cd $HOME_DIR
done < $file

